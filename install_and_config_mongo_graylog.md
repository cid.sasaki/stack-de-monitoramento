

# Elasticsearch 
   1. Instalação
   
      Instalar o Java
      ```
      yum -y install java-1.8.0-openjdk-headless.x86_64
      ```
      
      Configura a crypto policies e importa a chave
      ```
      update-crypto-policies --set DEFAULT:SHA1
   
      rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
      
      ```
      
      Configura o repositório para a versão 7.10.2
      ```
      [logstash-7.x]
      name=Elastic repository for 7.x packages
      baseurl=https://artifacts.elastic.co/packages/7.x/yum
      gpgcheck=1
      gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
      enabled=1
      autorefresh=1
      type=rpm-md
      ```
      
      Instala o Elasticsearch
      ```
      sudo yum install elasticsearch-oss
      ```
   
   2. Configuração
   
      2.1 Configurar a JVM para 50% da memória do host/vm 
      ```
      $ vim /etc/elasticsearch/jvm.options
      -Xms8Gi
      -Xmx8Gi
      ```
   
      2.2 Editar o hosts e inserir os servidores
      ```
      0.0.0.1   hostname0001.dominio     hostname0001
      0.0.0.2   hostname0002.dominio     hostname0002
      0.0.0.3   hostname0003.dominio     hostname0003
      0.0.0.4   hostname0004.dominio     hostname0004
      ```
      
      2.3 Configuração do cluster do elasticsearch
      ```
      $ vim /etc/elasticsearch/elasticsearch.yml
      cluster.name: graylog
      node.name: ${HOSTNAME}
      network.host: 0.0.0.0
      http.port: 9200
      discovery.zen.ping.unicast.hosts: ["hostname0001.dominio", "hostname0002.dominio", "hostname0003.dominio", "hostname0004.dominio"]
      ```
   
      2.4 Após a instalação e configuração, basta subir os serviço com os seguintes comandos
      ```
      systemctl daemon-reload
      systemctl enable elasticsearch.service
      systemctl restart elasticsearch.service 
      ```
      2.5 Verificar o status do serviço
      ```
      systemctl status elasticsearch.service
      ```
      





# MongoDB

   1. Instalação

      Agora vamos configurar três servidores com MongoDB e Graylog, então recomendo no mínimo 15Gb de RAM.

      Instale o MongoDB Community Edition de acordo com a documentação oficial
      ```
      #Crie o repositório em /etc/yum.repos.d/mongodb-org-6.0.repo com o conteudo abaixo
   
      [mongodb-org-6.0]
      name=MongoDB Repository
      baseurl=https://repo.mongodb.org/yum/redhat/9/mongodb-org/6.0/x86_64/
      gpgcheck=1
      enabled=1
      gpgkey=https://pgp.mongodb.com/server-6.0.asc
   
      atualize para pegar a nova configuração do repositório
      # yum update
   
      ```

      Instale os pacotes MongoDB
      ```
      sudo yum install -y mongodb-org-6.0.14 mongodb-org-database-6.0.14 mongodb-org-server-6.0.14 mongodb-org-mongos-6.0.14 mongodb-org-tools-6.0.14
      ```

      Fixe a versão do pacote, para que não seja atualizado pelo YUM, adicionando a seguinte excludediretiva ao seu /etc/yum.confarquivo:
      ```   
      exclude=mongodb-org,mongodb-org-database,mongodb-org-server,mongodb-mongosh,mongodb-org-mongos,mongodb-org-tools
      ```

   2. Configuração
   
      Seguir o passo a passo abaixo, para configurar corretamente o MongoDB em Réplica Set
   
      ```
      #Configure as permissões do usuário mongod:
      mkdir -p /var/lib/mongo
      mkdir -p /var/log/mongodb
   
      #Configure as permissões do usuário mongod:
      chown -R mongod:mongod /var/lib/mongo
      chown -R mongod:mongod /var/log/mongodb
   
      ```


      Verificar a configuração dos arquivos mongo.conf e mongod.service
   

      Arquivo de configuração /etc/mongod.conf
      ```
      # mongod.conf
      
      # for documentation of all options, see:
      #   http://docs.mongodb.org/manual/reference/configuration-options/
      
      # where to write logging data.
      systemLog:
        destination: file
        logAppend: true
        path: /var/log/mongodb/mongod.log
      
      # Where and how to store data.
      storage:
        dbPath: /var/lib/mongo
        journal:
          enabled: true
      #  engine:
      #  wiredTiger:
      
      # how the process runs
      processManagement:
        timeZoneInfo: /usr/share/zoneinfo
      
      # network interfaces
      net:
        port: 27017
        bindIp: 127.0.0.1,hostname0001.dominio  # Enter 0.0.0.0,:: to bind to all IPv4 and IPv6 addresses or, alternatively, use the net.bindIpAll setting.
      
      
      #security:
      
      #operationProfiling:
      
      replication:
        replSetName: "rs0"
      #sharding:
      
      ## Enterprise-Only Options
      
      #auditLog:
      
      #snmp:

      ```

      Service /usr/lib/systemd/system/mongod.service
      
      ```
      [Unit]
      Description=MongoDB Database Server
      Documentation=https://docs.mongodb.org/manual
      After=network-online.target
      Wants=network-online.target
      
      [Service]
      User=mongod
      Group=mongod
      Environment="OPTIONS=--fork -f /etc/mongod.conf"
      #Environment="MONGODB_CONFIG_OVERRIDE_NOFORK=1"
      EnvironmentFile=-/etc/sysconfig/mongod
      ExecStart=/usr/bin/mongod $OPTIONS
      ExecStartPre=/usr/bin/mkdir -p /mongo/data/db
      ExecStartPre=/usr/bin/chown mongod:mongod /mongo/data/db
      ExecStartPre=/usr/bin/chmod 0755 /mongo/data/db
      PermissionsStartOnly=true
      #PIDFile=/mongo/mongodb_pid/mongod.pid
      Type=forking
      
      [Unit]
      Description=MongoDB Database Server
      Documentation=https://docs.mongodb.org/manual
      After=network-online.target
      Wants=network-online.target
      
      [Service]
      User=mongod
      Group=mongod
      Environment="OPTIONS=--fork -f /etc/mongod.conf"
      #Environment="MONGODB_CONFIG_OVERRIDE_NOFORK=1"
      EnvironmentFile=-/etc/sysconfig/mongod
      ExecStart=/usr/bin/mongod $OPTIONS
      ExecStartPre=/usr/bin/mkdir -p /mongo/data/db
      ExecStartPre=/usr/bin/chown mongod:mongod /mongo/data/db
      ExecStartPre=/usr/bin/chmod 0755 /mongo/data/db
      PermissionsStartOnly=true
      #PIDFile=/mongo/mongodb_pid/mongod.pid
      Type=forking
      
      ```   
      Iniciar o serviço
      ```
      systemctl daemon-reload
      systemctl enable mongod
      systemctl start mongod
   
      #Você consegue testar a conexão executando o comando:
      mongosh
      ```
   
   3. Para a configuração das réplicas do MongoDB basta seguir o passo a passo abaixo (antes é preciso parar a instância do mongod que está em execução):
      rodar em todos os nodes
      Para configurar o ReplicaSet, adicione no arquivo do `mongod.conf` :
      ```
      echo -e "replication:\n  replSetName: \"rs0\"" | sudo tee -a /etc/mongod.conf
      ```

      executar o comando abaixo somente em um node (no primeiro node)
      ```
      rs.initiate( {
         _id : "rs0",
         members: [
            { _id: 0, host: "hostname1:27017" },
            { _id: 1, host: "hostname2:27017" },
            { _id: 2, host: "hostname3:27017" }
         ]
      })   
      # Crie um banco de dados e um usuário para o Graylog:
      
      use graylog;
      db.createUser( { user: "mongo_admin", pwd: "graylog", roles: [ { role: "root", db: "admin" } ] } )
      ```   
   
   
   
   
# Graylog Servers



   1. Instalação do Graylog
      ```     
      rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-5.2-repository_latest.rpm
   
      yum install graylog-server
      ```

   2. Configuração do Graylog

      Gerar password_secret com PWGEN

      ```  
      wget https://yum.oracle.com/repo/OracleLinux/OL9/developer/EPEL/x86_64/getPackage/pwgen-2.08-8.el9.x86_64.rpm
   
      [root@hostname]# pwgen -N 1 -s 96
   
      ```
